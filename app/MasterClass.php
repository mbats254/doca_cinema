<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masterclass extends Model
{
    protected $fillable = [
        'name', 'vimeo_link', 'overview','uniqid','poster','category'
    ];
}
