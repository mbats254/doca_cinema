<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{

    protected $fillable = [
        'name', 'country', 'about','films','facebook','twitter','google_plus','youtube','uniqid','photo'
    ];
}
