<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthAdminFinal extends Model
{
    protected $fillable = [
        'user_uniqid','status'
    ];
}
