<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class NewSubscriberNotification extends Notification
{
    use Queueable;
    protected $admin;
    protected $subscriber;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($admin,$subscriber)
    {
        $this->admin = $admin;
        $this->subscriber = $subscriber;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hello '.$this->admin->name)
                    ->line('A new user has just subscribed to our newsletter.')
                    ->line(new HtmlString('<a href=mailto:'.$this->subscriber->email.' alt=pineapple>Subscriber</a>'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
