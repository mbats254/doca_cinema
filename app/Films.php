<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Films extends Model
{

     protected $fillable = [
        'name', 'poster', 'rating','vimeo_link','uniqid','writer','director','release_date','language','overview','category','time'
    ];
}
