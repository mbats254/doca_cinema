<?php

namespace App\Http\Controllers;

use App\Beneficiary;
use App\Category;
use Illuminate\Http\Request;
use \App\Films;
use App\Workshop;
use App\MasterClass;
use App\Subscribers;
use Ap\Beneficiaries;
use App\Comment;
use App\Notifications\SubscriptionNotification;
use App\Notifications\NewSubscriberNotification;

class ContentController extends Controller
{
    public $successStatus = 200;/**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function all_films(Request $request)
    {
        $films = Films::get();
        return response()->json(['success' => $films], $this->successStatus);
    }

    public function all_masterclasses(Request $request)
    {
        $masterclasses = MasterClass::get();
        return response()->json([$masterclasses], $this->successStatus);
    }

    public function all_workshops(Request $request)
    {
        $workshops = Workshop::get();
        return response()->json([$workshops], $this->successStatus);
    }

    public function all_beneficiaries(Request $request)
    {
        $beneficiaries = Beneficiary::get();
        return response()->json([$beneficiaries], $this->successStatus);
    }

    public function all_categories(Request $request)
    {
        $categories = Category::get();
        return response()->json(['success' => $categories], $this->successStatus);

    }

    public function single_film(Request $request)
    {
        $film = Film::where('uniqid','=',$request->uniqid)->get()->first();
        return response()->json(['success' => $film], $this->successStatus);
    }

    public function single_masterclass(Request $request)
    {
        $masterclass = MasterClass::where('uniqid','=',$request->uniqid)->get()->first();
        return response()->json(['success' => $masterclass], $this->successStatus);
    }

    public function single_workshop(Request $request)
    {
        $workshop = Workshop::where('uniqid','=',$request->uniqid)->get()->first();
        return response()->json(['success' => $workshop], $this->successStatus);
    }

    public function single_beneficiary(Request $request)
    {
        $beneficiary = Beneficiary::where('uniqid','=',$request->uniqid)->get()->first();
        return response()->json(['success' => $beneficiary], $this->successStatus);
    }

    public function single_category(Request $request)
    {
        $category = Category::where('uniqid','=',$request->uniqid)->get();
        return response()->json(['success' => $category], $this->successStatus);
    }

    public function subscribe_newsletter(Request $request)
    {
        $response = $request->all();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:subscribers',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $subscriber = Subscribers::updateorCreate([
            'email' => $request->email,
            'uniqid' => uniqid()
        ]);
        $subscriber->notify(new SubscriptionNotification($subscriber));
        $all_admin = Admin::get();
        foreach($all_admin as $admin)
        {
            $admin->notify(new NewSubscriberNotification($admin,$subscriber));
        }
        if ($subscriber) {
            return response()->json(1);
         } else {
            return response()->json(0);
         }
    }

    public function post_comments(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

   $comment = Comment::updateorCreate([
       'message' => $request->message,
       'user_email' => \Auth::user()->email,
       'content_uniqid' => $request->uniqid,
       'uniqid' => uniqid()

   ]);

   return response()->json(['success' => $comment], $this->successStatus);
    }

    public function single_comment(Request $request)
    {
        $comment = Comment::where('uniqid','=',$request->uniqid)->get()->first();
        return response()->json(['success' => $comment], $this->successStatus);
    }

    public function all_comments(Request $request)
    {
        $comments = Comment::get();
        return response()->json(['success' => $comments], $this->successStatus);
    }

    public function search_masterclass(Request $request)
    {
        $masterclasses = MasterClass::where('name','like',$request->search_input)->orwhere('category','like',$request->search_input)->get();
        $beneficiaries = Beneficiaries::where('name','like',$request->search_input)->orwhere('country','like',$request->search_input)->get();
        $films = Films::where('name','like',$request->search_input)->orwhere('writer','like',$request->search_input)->orwhere('director','like',$request->search_input)->orwhere('category','like',$request->search_input)->orwhere('overview','like',$request->search_input)->orwhere('language','like',$request->search_input)->get();
        $workshops = Workshop::where('name','like',$request->search_input)->orwhere('description','like',$request->search_input)->orwhere('venue','like',$request->search_input)->get();
    }
}
