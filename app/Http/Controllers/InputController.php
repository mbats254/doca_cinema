<?php

namespace App\Http\Controllers;

use App\Beneficiary;
use Illuminate\Http\Request;
use \App\Category;
use \App\Films;
use \App\MasterClass;
use \App\Workshop;
use \App\Newsletter;
use App\Subscribers;
use App\AuthAdminFinal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Notifications\NewsletterNotification;

use App\Notifications\NewSubscriberNotification;

class InputController extends Controller
{
    public function input_films(Request $request)
    {
        $categories = Category::where('item','=','Films')->get();
        return view('films.add_films',compact('categories'));
    }

    public function input_category(Request $request)
    {
        return view('add_category');
    }

    public function post_category(Request $request)
    {
        $category = Category::updateorCreate([
            'name' => $request->name,
            'uniqid' => uniqid(),
            'item' => $request->item
        ]);

        Log::info("Category Successfully Posted");
        $request->session()->flash("success","Category Successfully Posted");
        return redirect()->back();
    }

    public function post_films(Request $request)
    {
        $poster = $request->file('poster');
        $uniqid = uniqid();
        $request->file('poster')->move(base_path().'/public/Film_Posters', $file_name = '/Film_Posters/'.str_replace(" ", "_", $request->name) . "." . $poster->getClientOriginalExtension());
        $film = Films::updateorCreate([
            'name' => $request->name,
            'poster' => $file_name,
            'vimeo_link' => $request->vimeo_link,
            'director' => $request->director,
            'writer' => $request->writer,
            'category' => $request->category,
            'language' => implode(',',$request->language),
            'overview' => $request->overview,
            'time' => $request->time,
            'rating' => $request->rating,
            'uniqid' => $uniqid,
            'release_date' => $request->release_date
        ]);

        Log::info($film->name." Successfully Posted");
        $request->session()->flash("success",$film->name." Successfully Posted");
        return redirect()->back();
    }

    public function input_masterclasses(Request $request)
    {
        $categories = Category::get();
        return view('masterclasses.add_masterclass',compact('categories'));
    }

    public function post_masterclasses(Request $request)
    {
        $poster = $request->file('poster');
        $uniqid = uniqid();
        $request->file('poster')->move(base_path().'/public/MasterClasses_Posters', $file_name = '/MasterClasses_Posters/'.str_replace(" ", "_", $request->name) . "." . $poster->getClientOriginalExtension());
        $masterclass = MasterClass::updateorCreate([
            'name' => $request->name,
            'poster' => $file_name,
            'vimeo_link' => $request->vimeo_link,
            'category' => $request->category,
            'uniqid' => $uniqid,
            'overview' => $request->overview,


        ]);

        Log::info($masterclass->name." Successfully Posted");
        $request->session()->flash("success",$masterclass->name." Successfully Posted");
        return redirect()->back();
    }

    public function input_workshops(Request $request,$auth_token=null)
    {
        if($auth_token !== null)
        {
            // $uniqid = explode('',$auth_token);
            $check = AuthAdminFinal::where('user_uniqid','=',$auth_token)->get();
            if($check !== null)
            {
                Log::info('authetication required');
                $request->session()->flash("error","authetication required");
                return redirect()->route('login');
            }
            $auth_admin = AuthAdminFinal::create([
                'user_uniqid' => $auth_token
            ]);
            if($auth_token[0] == '5' && strlen($auth_token) == 13)
            {
                return view('workshops.add_workshop');
            }

        }
        else if(Auth::user()->admin !== null)
        {
            // dd('kkk');
            return view('workshops.add_workshop');
        }
        else {
            Log::info('authetication required');
            $request->session()->flash("error","authetication required. we need to make sure you are authorised to carry out tasks here");
            return redirect('/');
        }
        // if(Auth::user()->admin )

    }

    public function post_workshops(Request $request)
    {

        $poster = $request->file('poster');
        $uniqid = uniqid();
        $request->file('poster')->move(base_path().'/public/Workshops_Posters', $file_name = '/Workshops_Posters/'.str_replace(" ", "_", $request->name) . "." . $poster->getClientOriginalExtension());
        $workshop = Workshop::updateorCreate([
            'date' => $request->date,
            'name' => $request->name,
            'poster' => $file_name,
            'description' => $request->description,
            'uniqid' => $uniqid,
        ]);

        Log::info($workshop->name." Successfully Posted");
        $request->session()->flash("success",$workshop->name." Successfully Posted");
        return redirect()->back();
    }

    public function input_beneficiaries(Request $request)
    {
        $films = Films::get();
        return view('beneficiaries.add_beneficiary',compact('films'));
    }

    public function post_beneficiaries(Request $request)
    {

        $photo = $request->file('photo');
        $uniqid = uniqid();
        $request->file('photo')->move(base_path().'/public/Beneficiary_Photo', $file_name = '/Beneficiary_Photo/'.str_replace(" ", "_", $request->name) . "." . $photo->getClientOriginalExtension());
        $beneficiary = Beneficiary::updateorCreate([
            'name' => $request->name,
            'country' => $request->country,
            'about' => $request->about,
            'films' => implode(',',$request->films),
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'google_plus' => $request->google_plus,
            'youtube' => $request->youtube,
            'uniqid' => $uniqid,
            'photo' => $file_name
        ]);

        Log::info($beneficiary->name." Successfully Posted");
        $request->session()->flash("success",$beneficiary->name." Successfully Posted");
        return redirect()->back();
    }

    public function input_newsletter(Request $request)
    {
        return view('input_newsletter');
    }

    public function post_newsletter(Request $request)
    {
        $uniqid = uniqid();
        $newsletter = Newsletter::updateorCreate([
            'title' => $request->Title,
            'newsletter' => $request->Newsletter,
            'uniqid' => $uniqid,
                    ]);
            // dd($newsletter);

        $subscribers = Subscribers::get();
        foreach($subscribers as $values => $subscriber)
        {
            $subscriber->notify(new NewsletterNotification($newsletter));
        }
        Log::info($request->Title." Sent Successfully");
        $request->session()->flash("success",$request->Title." Sent Successfully");
        return redirect()->back();
    }


}
