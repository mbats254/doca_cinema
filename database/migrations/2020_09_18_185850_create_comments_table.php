<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // protected $fillable = [
        //     'message', 'user_email', 'uniqid', 'content_uniqid'
        // ];
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->longText('message');
            $table->string('user_email');
            $table->string('uniqid');
            $table->string('content_uniqid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
