<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/all/films','ContentController@all_films')->name('all.films');
Route::get('/all/masterclasses','ContentController@all_masterclasses')->name('all.masterclasses');
Route::get('/all/workshops','ContentController@all_workshops')->name('all.workshops');
Route::get('/all/beneficiaries','ContentController@all_beneficiaries')->name('all.beneficiaries');
Route::get('/all/categories','ContentController@all_categories')->name('all.categories');
Route::get('/single/film/{uniqid}','ContentController@single_film')->name('single.film');
Route::get('/single/masterclass/{uniqid}','ContentController@single_masterclass')->name('single.masterclass');
Route::get('/single/workshop/{uniqid}','ContentController@single_workshop')->name('single.workshop');
Route::get('/single/beneficiary/{uniqid}','ContentController@single_beneficiary')->name('single.beneficiary');
Route::get('/single/category/{uniqid}','ContentController@single_category')->name('single.category');
Route::post('/subscribe/newsletter','ContentController@subscribe_newsletter')->name('subscribe.newsletter');
Route::post('/post/comment','ContentController@post_comments')->name('post.comments');
Route::get('/single/comment/{uniqid}','ContentController@single_comment')->name('single.comment');
Route::get('/all/comments/{uniqid}','ContentController@all_comments')->name('all.comment');
Route::get('/search/all/{search_input}','ContentController@search_masterclass')->name('search.masterclasses');

