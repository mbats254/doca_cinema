<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/input/workshops/{auth_token?}', 'InputController@input_workshops')->name('input.workshop');
Route::post('/post/workshops', 'InputController@post_workshops')->name('post.workshops');
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/add/films', 'InputController@input_films')->name('input.films');
    Route::post('/post/films', 'InputController@post_films')->name('film.post');
    Route::get('/add/masterclasses', 'InputController@input_masterclasses')->name('input.masterclasses');
    Route::post('/post/masterclasses', 'InputController@post_masterclasses')->name('masterclasses.post');
    Route::get('/input/beneficiaries', 'InputController@input_beneficiaries')->name('input.beneficiaries');
    Route::post('/post/beneficiaries', 'InputController@post_beneficiaries')->name('beneficiaries.post');
    Route::get('/input/category', 'InputController@input_category')->name('input.category');
    Route::get('/input/newsletter', 'InputController@input_newsletter')->name('input.newsletter');
    Route::post('/post/newsletter', 'InputController@post_newsletter')->name('newsletter.post');
    Route::post('/post/category', 'InputController@post_category')->name('category.post');
});
