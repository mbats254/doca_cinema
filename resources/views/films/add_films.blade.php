

@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('users.partials.header', ['title' => __('Add Film')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Add Film') }}</h3>
                            </div>
                            <div class="col-4 text-right">

                            </div>
                        </div>
                    <div class="card-body">
                            <form method='POST' action='{{ route('film.post') }}' enctype="multipart/form-data">
                                    @csrf

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Film Name') }}</label>
                                        <input type="text" name="name" placeholder="E.g. Katikati"  class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                    </div>

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Film Poster') }}</label>
                                        <input type="file" name="poster" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required accept=".png,.jpg,.gif,.JPEG" autofocus>
                                    </div>

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Vimeo Link') }}</label>
                                        <input type="text" name="vimeo_link" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                    </div>
                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Director') }}</label>
                                        <input type="text" name="director" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                    </div>
                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Writer') }}</label>
                                        <input type="text" name="writer" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                    </div>
                                    <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Category') }}</label>
                                        <select class="form-control" name="category">
                                            @foreach($categories as $category=>$values)
                                            <option value="{!! $values->name !!}">{!! $values->name !!}</option>
                                          @endforeach
                                        </select>
                                    </div>
                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Language') }}</label>
                                        <select class="js-example-basic-multiple select2 form-control" name="language[]" multiple="multiple">

                                            <option>English</option>
                                            <option>French</option>
                                            <option>German</option>
                                            <option>Swahili</option>

                                        </select>
                                    </div>
                                    <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Overview') }}</label>
                                        <textarea name="overview" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus></textarea>
                                    </div>

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                    <label class="form-control-label">{{ __('Time') }}</label>
                                    <input type="number" name="time" placeholder="Time in Minutes"  class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                </div>

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                    <label class="form-control-label">{{ __('Release Date') }}</label>
                                    <input type="date" name="release_date" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                </div>

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Rating') }}</label>
                                        <input type="number" name="rating" min="1" max="10" placeholder="Rate out of 10"  class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                    </div>


                                <input type="submit" class="btn btn-success mt-4" value="Submit">

                                </form>
                                {{-- <button class="btn btn-success mt-4 add_field" id="this">Add Field</button> --}}
                             </div>

                </div>
            </div>
        </div>
<script type="text/javascript">
$(document).ready(function(){
    var count = 0;
    $('.add_field').click(function(){
        count ++;
        $('.field_number').val(count)
        var input = '<div class=form-group><label class=form-control-label>New Field</label><input type=text name=field_'+count+' placeholder=one-page  class=form-control placeholder=School Details required autofocus></div>';
        $('.input').append(input);

    });

// var tagname = $('.add_field').prop("className");
});

</script>
        @include('layouts.footers.auth')
    </div>
@endsection







