

@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('users.partials.header', ['title' => __('Add Category')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Add Category') }}</h3>
                            </div>
                            <div class="col-4 text-right">

                            </div>
                        </div>
                    <div class="card-body">
                            <form method='POST' action='{{ route('category.post') }}' enctype="multipart/form-data">
                                    @csrf

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Category Name') }}</label>
                                        <input type="text" name="name" placeholder="E.g. Action"  class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                    </div>

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Item Name') }}</label>
                                        <select name="item" class="form-control">
                                            <option>Films</option>
                                            <option>Masterclasses</option>
                                            <option>Workshops</option>
                                        </select>
                                    </div>

                                <input type="submit" class="btn btn-success mt-4" value="Submit">

                                </form>
                                {{-- <button class="btn btn-success mt-4 add_field" id="this">Add Field</button> --}}
                             </div>

                </div>
            </div>
        </div>
<script type="text/javascript">
$(document).ready(function(){
    var count = 0;
    $('.add_field').click(function(){
        count ++;
        $('.field_number').val(count)
        var input = '<div class=form-group><label class=form-control-label>New Field</label><input type=text name=field_'+count+' placeholder=one-page  class=form-control placeholder=School Details required autofocus></div>';
        $('.input').append(input);

    });

// var tagname = $('.add_field').prop("className");
});

</script>
        @include('layouts.footers.auth')
    </div>
@endsection







