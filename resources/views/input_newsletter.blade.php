

@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('users.partials.header', ['title' => __('Newsletter')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Add Newsletter Message') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{-- <a href="{{ route('all.newsletter.messages') }}" class="btn btn-sm btn-primary">{{ __('Newsletter Messages') }}</a> --}}
                            </div>
                        </div>
                    <div class="card-body">
                            <form method='POST' action='{{ route('newsletter.post') }}' enctype="multipart/form-data">
                                    @csrf

                                <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                        <label class="form-control-label">{{ __('Title') }}</label>
                                        <input type="text" name="Title"  class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus>
                                    </div>



                                        <div class="form-group{{ $errors->has('application_form') ? ' has-danger' : '' }}">
                                            <label class="form-control-label">{{ __('Newsletter Body') }}</label>
                                            <textarea name="Newsletter"  class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus></textarea>
                                        </div>


                                    <input type="submit" class="btn btn-success mt-4" value="Submit">

                                </form>
                                {{-- <button class="btn btn-success mt-4 add_field" id="this">Add Field</button> --}}
                             </div>

                </div>
            </div>
        </div>
<script type="text/javascript">
$(document).ready(function(){
    var count = 0;
    $('.add_field').click(function(){
        count ++;
        $('.field_number').val(count)
        var input = '<div class=form-group><label class=form-control-label>New Field</label><input type=text name=field_'+count+' placeholder=one-page  class=form-control placeholder=School Details required autofocus></div>';
        $('.input').append(input);

    });

// var tagname = $('.add_field').prop("className");
});

</script>
        @include('layouts.footers.auth')
    </div>
@endsection







